package fr.ulille.iut.tva.service;

//import jakarta.ws.rs.*;

/**
 * TauxTva
 */
public enum TauxTva {
    NORMAL(20d), INTERMEDIAIRE(10d), REDUIT(5.5), PARTICULIER(2.1);

    public final double taux;

    private TauxTva(double taux) {
        this.taux = taux;
    }

    public static TauxTva fromString(String label) {
        return TauxTva.valueOf(label);
    }

}
