package fr.ulille.iut.tva.ressource;

import java.util.ArrayList;
import java.util.List;

import exception.NiveauTvaInexistantException;
import fr.ulille.iut.tva.dto.InfoTauxDto;
import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;

/**
 * TvaRessource
 */
@Path("tva")
public class TvaRessource {
    private CalculTva calculTva = new CalculTva();

    @GET
    @Path("tauxpardefaut")
    public double getValeurTauxParDefaut() {
        return TauxTva.NORMAL.taux;
    }

    @GET
    @Path("valeur/{niveauTva}")
    public double getValeurTaux(@PathParam("niveauTva") String niveau) {
        try {
            return TauxTva.valueOf(niveau.toUpperCase()).taux;
        } catch (Exception e) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("details/{niveauTva}")
    public double getMontantTotal(@PathParam("niveauTva") String niveau, @QueryParam("somme") String value) {
        try {

            double s = Double.parseDouble(value);
            return s + (TauxTva.valueOf(niveau.toUpperCase()).taux * s) + 2;

        } catch (Exception e) {
            throw new NiveauTvaInexistantException();
        }
    }

    @GET
    @Path("lestaux")
    public List<InfoTauxDto> getInfoTaux() {
        ArrayList<InfoTauxDto> result = new ArrayList<InfoTauxDto>();
        for (TauxTva t : TauxTva.values()) {
            result.add(new InfoTauxDto(t.name(), t.taux));
        }
        return result;
    }

}
